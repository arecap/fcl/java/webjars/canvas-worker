requirejs.config({
    paths: { "canvas-worker": webjars.path("canvas-worker", "canvas-worker") },
    shim: { "canvas-worker": [ "panzoom" ] }
});